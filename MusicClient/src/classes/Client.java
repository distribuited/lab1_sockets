package classes;

public class Client {
    SocketClient socketClient;

    public Client(String address, int port) {
        socketClient = new SocketClient(address, port);
    }

    public void operation(String data) {
        this.socketClient.request(data);
    }
}
