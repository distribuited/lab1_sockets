package classes;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient {
    private InetAddress address;
    private int port;
    private Socket clientSk;
    private ObjectOutputStream out;
    private ObjectInputStream inp;

    public SocketClient(String address, int port) {
        try {
            this.port = port;
            this.address = InetAddress.getByName(address);
            this.out = null;
            this.inp = null;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    public void request(String data) {
        try {
            this.clientSk = new Socket(this.address, this.port);
            this.out = new ObjectOutputStream(this.clientSk.getOutputStream());
            this.out.flush();
            this.inp = new ObjectInputStream(this.clientSk.getInputStream());
            System.out.println("\n [Client]: Conexión exitosa.");
            send(data);
            while (true) {
                try {
                    data = (String) this.inp.readObject();
                    if (data == null) {
                        closeService();
                    } else {
                        System.out.println("\n [Client]: el servidor dice:" + data);
                    }
                } catch (Exception e) {
                    System.out.println("\n [Client]: No se puede recibir la data.");
                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    private void send(String data) {
        try {
            this.out.writeObject("[Client]: " + data);
            this.out.flush();
        } catch (Exception e) {
            System.out.println("\n [Client]: No se puede enviar la data.");
        }
    }

    private void closeService() {
        try {
            this.inp.close();
            this.out.close();
            this.clientSk.close();
            System.out.println("\n [Client]: Conexión terminada.");
        } catch (Exception e) {
            System.out.println("\n [Client]: No se puede cerrar la conexión.");
        }
    }
}
