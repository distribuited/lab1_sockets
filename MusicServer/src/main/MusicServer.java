package main;

import classes.Server;
import classes.database.Database;

public class MusicServer {

    public static void main(String[] args) {
        Server server = new Server(1802);

        Database musicDatabase = new Database();
        server.run();
    }

}