package classes;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer {

    private int port;
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private ObjectOutputStream out;
    private ObjectInputStream inp;

    // constructor
    public SocketServer(int port) {
        try {
            this.port = port;
            this.serverSocket = new ServerSocket(port, 100);
            this.out = null;
            this.inp = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void listening() {
        try {
            String data;
            System.out.println("\n [MusicServer]: " + "Esperando a cliente \n");
            this.clientSocket = this.serverSocket.accept();
            this.out = new ObjectOutputStream(this.clientSocket.getOutputStream());
            this.out.flush();
            this.inp = new ObjectInputStream(this.clientSocket.getInputStream());
            System.out.println("\n [MusicServer]: " + "Conexión exitosa.\n");
            while (true) {
                try {
                    data = (String) this.inp.readObject();
                    if (data == null) {
                        closeService();
                    } else {
                        // aquí se recibe respuesta
                        send("Escuche esto: " + data);
                    }
                } catch (Exception e) {
                    System.out.println("\n [MusicServer]: No se puede recibir la data.");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void closeService() {
        try {
            this.inp.close();
            this.out.close();
            this.clientSocket.close();
            this.serverSocket.close();
            System.out.println("\n [MusicServer]: Conexión terminada.");
        } catch (Exception e) {
            System.out.println("\n [MusicServer]: No se puede cerrar la conexión.");
        }
    }

    private void send(String data) {
        try {
            this.out.writeObject("[MusicServer]: " + data);
            this.out.flush();
        } catch (Exception e) {
            System.out.println("\n [MusicServer]: No se puede enviar la data.");
        }
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
