package classes;

public class Server {

    private SocketServer socketServer;

    public Server(int port) {
        this.socketServer = new SocketServer(port);
    }

    public void run() {
        socketServer.listening();
    }
}
